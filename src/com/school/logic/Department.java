package com.school.logic;

import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
/**
 * 
 * @author 984493
 *
 */

public class Department {
	private String name;
	private List<Person> persons = new ArrayList<Person>(); //association
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Person> getPersons() {
		return persons;
	}

	public void setPersons(List<Person> persons) {
		this.persons = persons;
	}

	public Department(final String name){
		this.name = name;
	}
	
	public double getTotalSalary(){
		double salary = 0.0;
		for (Person p : persons) {
			if(p instanceof Faculty) {
				Faculty s = (Faculty) p;
				salary += s.getSalary();
			}
			else if(p instanceof IStaff) {
				IStaff s = (IStaff) p;
				salary += s.getSalary();
			}
		}
		return salary;
		
	}
	
	public void showAllMembers(){
		for (Person p : persons) {
			String type="student";
			if(p instanceof Staff){
				type = "staff";
			} else if (p instanceof Faculty){
				type = "faculty";
			}
			else if (p instanceof StaffStudent){
				type = "staff-student";
			}
			System.out.println(p.getName() + ", " + ", " + p.getPhone() + ", " +p.getAge() +", " + type);
		}
		return;
		
	}
	
	public void unitsPerFaculty(){
		for (Person person : persons) {
			if(person instanceof Faculty) {
				Faculty faculty = (Faculty) person;
				System.out.println("" + person.getName() + " : " + faculty.getTotalUnits());
			}
		}
		return;
	}
	
	public void addPerson(Person person){
		persons.add(person);
	}
	
   public List<Student> getStudents(String facultyName){
	   List<Student> students = new ArrayList<Student>();
	   Faculty searchFaculty = null;
	   for ( Person p : this.getPersons()) {
		   if (p instanceof Faculty && p.getName().equals(facultyName)) {
			   searchFaculty = (Faculty) p;
			   break;
		   }
	   }
	   
	   if(searchFaculty == null)
		   throw new IllegalArgumentException("No such faculty exists.");
	   
	   for ( Person p : this.getPersons()) {
		   if (p instanceof Student) {
			   Student student = (Student) p;
			   final List<Course> studentCourses = student.getCourses();
			   for(Course course : studentCourses) {
				   if(course.getFaculty().equals(searchFaculty)) {
					   students.add(student);
					   break;
				   }
			   }
		   }
	   }
	   
	   return students;
   }
}


