package com.school.logic;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author 984493
 *
 */

public class Faculty extends Person {
	private double salary;
	private List<Course> courses = new ArrayList<Course>();

	public Faculty(final String name, final String phone, final int age, final double salary){
		super(name, phone, age);
		this.salary = salary;
	}
	
	public void addCourse(Course course){
		courses.add(course);
	}
	
	public int getTotalUnits(){
		int units = 0;
		for(Course course : courses) {
			units += course.getUnits();
		}
		return units;
		
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public List<Course> getCourses() {
		return courses;
	}

	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}

}
