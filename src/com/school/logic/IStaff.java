package com.school.logic;
/**
 * 
 * @author 984493
 *
 */
public interface IStaff {

	double getSalary();

	void setSalary(double salary);

}
