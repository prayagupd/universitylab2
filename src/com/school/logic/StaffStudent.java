package com.school.logic;

import java.util.Date;
import java.util.List;

/**
 * 
 * @author 984493
 *
 */

public class StaffStudent extends Student implements IStaff {
	
	private double salary;
	private Date courseJoined;
	
	public StaffStudent(Student student, final double salary){
		super(student.getName(), student.getPhone(), student.getAge(), student.getGpa());
		this.salary = salary;
	}
	
	public StaffStudent(final String name, final String phone, final int age, final double gpa, final double salary){
		super(name, phone, age, gpa);
		this.salary = salary;
	}

	@Override
	public double getSalary() {
		return salary;
	}

	@Override
	public void setSalary(double salary) {
		this.salary = salary;
	}

	public Date getCourseJoined() {
		return courseJoined;
	}

	public void setCourseJoined(Date courseJoined) {
		this.courseJoined = courseJoined;
	}
	
	
}
