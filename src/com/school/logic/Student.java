package com.school.logic;

import java.util.List;

public class Student extends Person{
	
	private double gpa;
	private List<Course> courses = new java.util.ArrayList<Course>();
	
	public Student(final String name, final String phone, final int age, final double gpa){
		super(name, phone, age);
		this.gpa = gpa;
	}
	
	public double getGpa() {
		return gpa;
	}

	public void setGpa(double gpa) {
		this.gpa = gpa;
	}

	public List<Course> getCourses() {
		return courses;
	}

	public void setCourses(List<Course> courses) {
		this.courses = courses;
	}

	public int getTotalUnits(){
		return 0;
		
	}

	public void addCourse(Course course){
		courses.add(course);
	}
}
